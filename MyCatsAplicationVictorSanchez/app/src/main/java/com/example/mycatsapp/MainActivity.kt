package com.example.mycatsapp

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.mycatsapp.ui.model.CatUIModel
import com.example.mycatsapp.ui.screens.CatsViewModel
import com.example.mycatsapp.ui.theme.MyCatsAppTheme
import com.skydoves.landscapist.ImageOptions
import com.skydoves.landscapist.glide.GlideImage

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CatsApp()
        }
    }

    @Composable
    fun CatsApp(catsViewModel: CatsViewModel = viewModel()) {
        val uiState = catsViewModel.catsUiState
        MyCatsAppTheme() {
            Column(modifier = Modifier.background(Color.DarkGray)) {
                CatsList(catsUiModelList = uiState)
            }
        }
    }

    @Composable
    fun CatsList(catsUiModelList: List<CatUIModel>, modifier: Modifier = Modifier) {
        LazyColumn(
            modifier = modifier
        ) {
            items(catsUiModelList) { cat ->
                CatCard(catsUIModel = cat, modifier = Modifier.padding(8.dp))
            }
        }
    }

    @Composable
    fun CatCard(catsUIModel: CatUIModel, modifier: Modifier = Modifier) {
        val mContext = LocalContext.current
        Card(
            modifier = modifier.clickable {
                mContext.startActivity(
                    Intent(
                        mContext,
                        CatDetailActivity::class.java
                    ).putExtra("CatUIModel", catsUIModel)
                )
            }, elevation = 5.dp
        ) {
            Column(
                modifier = Modifier
                    .background(color = Color.White)
            ) {
                Row(
                    modifier = Modifier.fillMaxSize(),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    GlideImage(
                        imageModel = { catsUIModel.image_url },
                        imageOptions = ImageOptions(contentScale = ContentScale.Crop),
                        modifier = Modifier
                            .size(200.dp)
                            .padding(8.dp)
                    )

                    Column {
                        Text(
                            text = catsUIModel.name,
                            modifier = Modifier.padding(10.dp),
                            fontFamily = FontFamily.Monospace,
                            fontSize = 20.sp,
                            overflow = TextOverflow.Ellipsis,
                            maxLines = 3,
                            fontWeight = FontWeight.Bold,
                            color = Color.Cyan
                        )

                        Text(
                            text = catsUIModel.description,
                            modifier = Modifier.padding(10.dp),
                            fontFamily = FontFamily.Monospace,
                            fontSize = 15.sp,
                            overflow = TextOverflow.Ellipsis,
                            maxLines = 3,
                            color = Color.DarkGray
                        )
                    }
                }
            }
        }
    }

    @Preview
    @Composable
    private fun AffirmationCardPreview() {
        CatCard(
            catsUIModel = CatUIModel(
                "",
                "",
                "",
                "",
                "",
                "",
                ""
            )
        )
    }

}