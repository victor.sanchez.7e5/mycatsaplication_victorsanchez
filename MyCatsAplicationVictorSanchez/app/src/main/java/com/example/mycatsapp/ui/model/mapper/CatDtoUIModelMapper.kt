package com.example.mycatsaplication.ui.model.mapper

import com.example.mycatsapp.data.apiservice.model.CatDto
import com.example.mycatsapp.data.apiservice.model.CatImageDto
import com.example.mycatsapp.ui.model.CatUIModel

class CatDtoUIModelMapper {
    fun map(listDto: List<CatDto>, imageDto : List<CatImageDto?>) : List<CatUIModel> {
        return mutableListOf<CatUIModel>().apply {
            for (i in listDto.indices){
                add(
                    CatUIModel(
                        listDto[i].id, listDto[i].name, listDto[i].temperament,
                        listDto[i].countryCode, listDto[i].description, listDto[i].wikipedia_url,
                        imageDto?.get(i)?.imageUrl ?: ""
                    )
                )
            }
        }
    }
}

