package com.example.mycatsapp.ui.screens

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mycatsapp.data.apiservice.CatApi
import com.example.mycatsapp.ui.model.CatUIModel
import com.example.mycatsaplication.ui.model.mapper.CatDtoUIModelMapper
import kotlinx.coroutines.launch

class CatsViewModel : ViewModel() {

    var catsUiState: List<CatUIModel> by mutableStateOf(emptyList())
        private set

    var mapper = CatDtoUIModelMapper()

    init {
        getCats()
    }

    private fun getCats() {
        viewModelScope.launch {
            val catsListDto = CatApi.retrofitService.getCatsRx()
            val imagesListDto = catsListDto.map { CatApi.retrofitService.getCatImageRx(it.id).firstOrNull() }
            catsUiState = mapper.map(catsListDto, imagesListDto)
        }
    }
}
