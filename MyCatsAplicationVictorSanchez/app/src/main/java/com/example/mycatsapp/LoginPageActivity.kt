package com.example.mycatsapp

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

class LoginActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            LoginPage()
        }
    }
}

@Composable
fun LoginPage() {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.White),
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        val username = remember { mutableStateOf(TextFieldValue()) }
        val password = remember { mutableStateOf(TextFieldValue()) }
        val mContext = LocalContext.current

        Text(
            text = "GATOS",
            style = TextStyle(
                fontSize = 40.sp,
                fontFamily = FontFamily.Default,
                color = Color.Cyan
            ),
            modifier = Modifier.padding(100.dp)
        )

        Spacer(modifier = Modifier.height(20.dp))
        TextField(
            label = { Text(text = "Nombre") },
            value = username.value,
            onValueChange = { username.value = it },
            colors = TextFieldDefaults.textFieldColors(backgroundColor = Color.White)
        )

        Spacer(modifier = Modifier.height(15.dp))
        TextField(
            label = { Text(text = "Contraseña") },
            value = password.value,
            visualTransformation = PasswordVisualTransformation(),
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
            onValueChange = { password.value = it },
            colors = TextFieldDefaults.textFieldColors(backgroundColor = Color.White)
        )

        Spacer(modifier = Modifier.height(15.dp))
        Box(
            modifier = Modifier
                .padding(50.dp)
        ) {
            Button(
                onClick = {
                    if (username.value.text != "" && password.value.text != "")
                        mContext.startActivity(Intent(mContext, MainActivity::class.java))
                    else
                        Toast.makeText(mContext, "Indroduce cosas", Toast.LENGTH_SHORT).show()
                },
                shape = RoundedCornerShape(50.dp),
                modifier = Modifier
                    .fillMaxWidth()
                    .height(50.dp),
                colors = ButtonDefaults.buttonColors(backgroundColor = Color.Cyan)
            ) {
                Text(text = "Login")
            }
        }
    }
}