package com.example.mycatsapp.data.apiservice

import com.example.mycatsapp.data.apiservice.model.CatDto
import com.example.mycatsapp.data.apiservice.model.CatImageDto
import retrofit2.Retrofit
import retrofit2.http.GET
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import retrofit2.http.Query

private const val BASE_URL = "https://api.thecatapi.com"

private val retrofit = Retrofit.Builder()
    .addConverterFactory(Json { ignoreUnknownKeys = true}.asConverterFactory(MediaType.get("application/json")))
    .baseUrl(BASE_URL)
    .build()

interface CatApiService {
    @GET("./v1/breeds")
    suspend fun getCatsRx(): List<CatDto>

    @GET("./v1/images/search")
    suspend fun getCatImageRx(@Query("breed_id") id: String): List<CatImageDto>

}

object CatApi {
    val retrofitService : CatApiService by lazy {
        retrofit.create(CatApiService::class.java)
    }
}


