package com.example.mycatsapp.ui.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class CatUIModel (
    val id: String,
    val name: String ,
    val temperament: String,
    val countryCode: String,
    val description: String,
    val wikipedia_url: String,
    val image_url : String
) : Parcelable